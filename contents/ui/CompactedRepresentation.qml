
import QtQuick 2.4
import QtQuick.Layouts 1.1
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PlasmaComponents3
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.kcoreaddons 1.0 as KCoreAddons

Item {
    id: compactedRepresentation
    Layout.minimumWidth: text.width
    

    readonly property int controlSize: units.iconSizes.large

    property double position: mpris2Source.currentData.Position || 1
    readonly property real rate: mpris2Source.currentData.Rate || 1
    readonly property double length: currentMetadata ? currentMetadata["mpris:length"] || 0 : 0
    readonly property bool canSeek: mpris2Source.currentData.CanSeek || false

    // only show hours (the default for KFormat) when track is actually longer than an hour
    readonly property int durationFormattingOptions: length >= 60*60*1000*1000 ? 0 : KCoreAddons.FormatTypes.FoldHours

    property bool disablePositionUpdate: false
    property bool keyPressed: false
    property var testVar
    property bool isSpotifyOpened: false

    function retrievePosition() {
        var service = mpris2Source.serviceForSource(mpris2Source.current);
        var operation = service.operationDescription("GetPosition");
        service.startOperationCall(operation);
    }

    Connections {
        target: plasmoid
        onExpandedChanged: {
            if (plasmoid.expanded) {
                retrievePosition();
            }
        }
    }
    
    Connections {
        target: mpris2Source
        onSourcesChanged: {
            var sources = mpris2Source.sources
            var spotifyFound = false
            for (var i = 0, length = sources.length; i < length; ++i) {
                var source = sources[i]
                if(source === "spotify") {
                    spotifyFound = true;
                    break
                }
            }
            isSpotifyOpened = spotifyFound
        }
    }

    onPositionChanged: {
        if (!seekSlider.pressed && !keyPressed) {
            disablePositionUpdate = true
            seekSlider.value = position
            disablePositionUpdate = false
        }
    }

    onLengthChanged: {
        disablePositionUpdate = true
        seekSlider.value = 0
        seekSlider.to = length
        retrievePosition()
        disablePositionUpdate = false
    }

    Keys.onPressed: keyPressed = true

    Keys.onReleased: {
        keyPressed = false

        if (!event.modifiers) {
            event.accepted = true

            if (event.key === Qt.Key_Space || event.key === Qt.Key_K) {
                // K is YouTube's key for "play/pause" :)
                root.togglePlaying()
            } else if (event.key === Qt.Key_P) {
                root.action_previous()
            } else if (event.key === Qt.Key_N) {
                root.action_next()
            } else if (event.key === Qt.Key_S) {
                root.action_stop()
            } else if (event.key === Qt.Key_Left || event.key === Qt.Key_J) { // TODO ltr languages
                // seek back 5s
                seekSlider.value = Math.max(0, seekSlider.value - 5000000) // microseconds
                seekSlider.moved();
            } else if (event.key === Qt.Key_Right || event.key === Qt.Key_L) {
                // seek forward 5s
                seekSlider.value = Math.min(seekSlider.to, seekSlider.value + 5000000)
                seekSlider.moved();
            } else if (event.key === Qt.Key_Home) {
                seekSlider.value = 0
                seekSlider.moved();
            } else if (event.key === Qt.Key_End) {
                seekSlider.value = seekSlider.to
                seekSlider.moved();
            } else if (event.key >= Qt.Key_0 && event.key <= Qt.Key_9) {
                // jump to percentage, ie. 0 = beginnign, 1 = 10% of total length etc
                seekSlider.value = seekSlider.to * (event.key - Qt.Key_0) / 10
                seekSlider.moved();
            } else {
                event.accepted = false
            }
        }
    }

    Item {
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: playerCombo.bottom
            bottom: controlCol.top
            margins: units.smallSpacing
        }

        PlasmaCore.IconItem {
            anchors {
                horizontalCenter: parent.horizontalCenter
                verticalCenter: parent.verticalCenter
            }

            height: Math.round(parent.height / 2)
            width: height

            source: mpris2Source.currentData["Desktop Icon Name"]
            visible: !albumArt.visible

            usesPlasmaTheme: false
        }
    }

    Text {
        id: text
        text: {
            if(root.state == "paused" || !isSpotifyOpened){
                return ""
            }

            return root.artist ? i18nc("artist – track", "%1 – %2", root.artist, root.track) : root.track
        }
        clip: true
        anchors.right: parent.right
        fontSizeMode: Text.VerticalFit
        anchors.rightMargin: 0
        anchors.verticalCenter: parent.verticalCenter
    }

    clip: true
}
